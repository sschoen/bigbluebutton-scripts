# bbb-scripts

Some helper scripts to install bigblubutton with bbb-export, netdata and prometheus and a scalelite server with grafana monitoring.#!/bin/bash

Mit dieser Anleitung lässt sich eine BigBlueButton-Infrastruktur auf mehreren Servern einrichten, die in der Lage 
sein sollte, mehrere hundert Teilnehmer zu versorgen. Dieses Setup lässt sich auch jederzeit auf mehrere tausend
Nutzer erweitern oder - wenn nicht mehr benötigt - reduzieren. Mit einem monatlichen finanziellen Aufwand von ca.
60 € sollte man aber dauerhaft rechnen (sonst lohnt der Aufwand nicht). Andererseits könnte man sich diesen mit 
mehreren Institutionen teilen, so lange eine gemeinsame Datenschutzvereinbarung getroffen wird.

Dafür wird zunächst ein scalelite-Lastserver installiert. Auf der gleichen Maschine wird ein Grafana-Monitoring-System
gehostet, mit dem alle BBBs überwacht werden.

Grundlage für alle Server sind gemietete root-Server (bei uns aus der Serverbörse der Firma Hetzner) mit eigener IP.
Zudem wird Zugang zu einem DNS-System benötigt, um den Zugriff über FQDNs zu ermöglichen.
Da es für BigBlueButton nicht empfohlen wird und - bis auf die Aufnahmen - die Server recht generisch sind, wird
auf Virtualisierung komplett verzichtet. Bis auf die Daten (Videos, Logdaten, Einstellungen) auf dem Lastserver 
lassen sich die Instanzen aber auch leicht wiederherstellen. 

Für dieses Skript gelten folgende Annahmen:
- Zugang zu einem DNS-System für die Domain mein.schule (und Ahnung, wie man hier A-Einträge setzt)
- Zugang zu einem Mailserver
- Linuxmuster.net LDAP (für die Greenlight-Anbindung).
- 3+ Hetzner-Server mit IP/Passwort.

Das Skript geht davon aus, dass vom linuxmuster-server als root gearbeitet wird. Falls einige Bedingungen erfüllt sind (insb. DNS),
sollte das Skript alles wichtige erledigen. Wenn jemand an irgendeiner Stelle Änderungen durchführt (z.B. Hostnamen ändern)
oder an irgendeiner Stelle etwas schief geht, dann kann es gut sein, dass nicht mehr alles funktioniert.

Voraussetzungen:

Zunächst müssen die root-Server gekauft/gemietet werden. Grundsätzliche Empfehlung: 4 Kerne, 32-64 GB Speicher. 
Mehr ist natürlich nicht schlecht. Für den Scalelite-Server möglichst große Festplatte (insb. für die Aufnahmen).

Im DNS sollten folgende Einträge eingerichtet werden:
$fqdn -> IP des Scalelite-Servers
bbb1.$domain, bbb2.$domain, ... -> IPs der BigBlueButton-Server

